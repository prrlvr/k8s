#!/bin/sh

filename="$PG_DB.bak"

echo "$HOST:$PORT:$PG_DB:$PG_USER:$PG_PASSWORD" >> .pgpass
pg_dump --no-password -U $PG_USER -h $HOST -p $PORT -d $PG_DB --format=c > $filename

# export to remote server
which ssh-agent || (apk update && apk add openssh-client)
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan porey-build.takima.io >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

scp -P 22 centos@porey-build.takima.io:~/db-save