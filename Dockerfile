FROM postgres:12.9-alpine
RUN apk add openssh

COPY db-dump.sh .

ENTRYPOINT [ "./db-dump.sh" ]